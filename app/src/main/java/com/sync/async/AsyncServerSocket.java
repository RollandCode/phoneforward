package com.sync.async;

public interface AsyncServerSocket {
    public void stop();
    public int getLocalPort();
}
