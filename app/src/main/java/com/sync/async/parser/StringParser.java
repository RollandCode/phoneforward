package com.sync.async.parser;

import com.sync.async.ByteBufferList;
import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.future.Future;
import com.sync.async.future.TransformFuture;

import java.lang.reflect.Type;
import java.nio.charset.Charset;


public class StringParser implements AsyncParser<String> {
    Charset forcedCharset;

    public StringParser() {
    }

    public StringParser(Charset charset) {
        this.forcedCharset = charset;
    }

    @Override
    public Future<String> parse(DataEmitter emitter) {
        final String charset = emitter.charset();
        return new ByteBufferListParser().parse(emitter)
        .then(new TransformFuture<String, ByteBufferList>() {
            @Override
            protected void transform(ByteBufferList result) throws Exception {
                Charset charsetToUse = forcedCharset;
                if (charsetToUse == null && charset != null)
                    charsetToUse = Charset.forName(charset);
                setComplete(result.readString(charsetToUse));
            }
        });
    }

    @Override
    public void write(DataSink sink, String value, CompletedCallback completed) {
        new ByteBufferListParser().write(sink, new ByteBufferList(value.getBytes()), completed);
    }

    @Override
    public Type getType() {
        return String.class;
    }
}
