package com.sync.async.parser;

import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.future.Future;

import java.lang.reflect.Type;

public interface AsyncParser<T> {
    Future<T> parse(DataEmitter emitter);
    void write(DataSink sink, T value, CompletedCallback completed);
    Type getType();
}
