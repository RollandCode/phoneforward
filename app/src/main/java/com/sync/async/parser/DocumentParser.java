package com.sync.async.parser;

import com.sync.async.ByteBufferList;
import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.future.Future;
import com.sync.async.future.TransformFuture;
import com.sync.async.http.body.DocumentBody;
import com.sync.async.stream.ByteBufferListInputStream;

import org.w3c.dom.Document;

import java.lang.reflect.Type;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class DocumentParser implements AsyncParser<Document> {
    @Override
    public Future<Document> parse(DataEmitter emitter) {
        return new ByteBufferListParser().parse(emitter)
        .then(new TransformFuture<Document, ByteBufferList>() {
            @Override
            protected void transform(ByteBufferList result) throws Exception {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                setComplete(db.parse(new ByteBufferListInputStream(result)));
            }
        });
    }

    @Override
    public void write(DataSink sink, Document value, CompletedCallback completed) {
        new DocumentBody(value).write(null, sink, completed);
    }

    @Override
    public Type getType() {
        return Document.class;
    }
}
