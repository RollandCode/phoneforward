package com.sync.async.http.server;

import com.sync.async.AsyncSocket;
import com.sync.async.DataEmitter;
import com.sync.async.http.Headers;
import com.sync.async.http.Multimap;
import com.sync.async.http.body.AsyncHttpRequestBody;

import java.util.regex.Matcher;

public interface AsyncHttpServerRequest extends DataEmitter {
    public Headers getHeaders();
    public Matcher getMatcher();
    public AsyncHttpRequestBody getBody();
    public AsyncSocket getSocket();
    public String getPath();
    public Multimap getQuery();
    public String getMethod();
}
