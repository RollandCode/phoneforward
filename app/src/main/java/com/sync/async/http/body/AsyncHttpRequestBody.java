package com.sync.async.http.body;

import com.sync.async.DataEmitter;
import com.sync.async.DataSink;
import com.sync.async.callback.CompletedCallback;
import com.sync.async.http.AsyncHttpRequest;

public interface AsyncHttpRequestBody<T> {
    public void write(AsyncHttpRequest request, DataSink sink, CompletedCallback completed);
    public void parse(DataEmitter emitter, CompletedCallback completed);
    public String getContentType();
    public boolean readFullyOnRequest();
    public int length();
    public T get();
}
