package com.sync.async.http.socketio;

public interface ErrorCallback {
    void onError(String error);
}
