package com.sync.async.http.socketio;

public interface ReconnectCallback {
    public void onReconnect();
}