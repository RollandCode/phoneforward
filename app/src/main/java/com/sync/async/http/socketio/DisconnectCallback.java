package com.sync.async.http.socketio;

public interface DisconnectCallback {
    void onDisconnect(Exception e);
}
