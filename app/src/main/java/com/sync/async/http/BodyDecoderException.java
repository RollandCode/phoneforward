package com.sync.async.http;

public class BodyDecoderException extends Exception {
    public BodyDecoderException(String message) {
        super(message);
    }
}
