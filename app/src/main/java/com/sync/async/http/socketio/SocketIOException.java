package com.sync.async.http.socketio;

public class SocketIOException extends Exception {
    public SocketIOException(String error) {
        super(error);
    }
}
