package com.sync.async.http.socketio;

public interface ExceptionCallback {
    public void onException(Exception e);
}
