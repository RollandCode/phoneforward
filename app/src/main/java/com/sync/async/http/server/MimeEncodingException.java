package com.sync.async.http.server;

public class MimeEncodingException extends Exception {
    public MimeEncodingException(String message) {
        super(message);
    }
}
