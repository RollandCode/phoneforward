package com.sync.async.http.socketio;

public interface StringCallback {
    public void onString(String string, Acknowledge acknowledge);
}