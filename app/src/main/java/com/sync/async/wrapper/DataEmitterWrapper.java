package com.sync.async.wrapper;

import com.sync.async.DataEmitter;

public interface DataEmitterWrapper extends DataEmitter {
    public DataEmitter getDataEmitter();
}
