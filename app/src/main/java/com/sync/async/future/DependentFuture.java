package com.sync.async.future;

public interface DependentFuture<T> extends Future<T>, DependentCancellable {
}
