package com.sync.async.future;


public interface FutureRunnable<T> {
    T run() throws Exception;
}
