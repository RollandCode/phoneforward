package com.sync.async.callback;

public interface WritableCallback {
    public void onWriteable();
}
