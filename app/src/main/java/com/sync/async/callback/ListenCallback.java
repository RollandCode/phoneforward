package com.sync.async.callback;

import com.sync.async.AsyncServerSocket;
import com.sync.async.AsyncSocket;


public interface ListenCallback extends CompletedCallback {
    public void onAccepted(AsyncSocket socket);
    public void onListening(AsyncServerSocket socket);
}
